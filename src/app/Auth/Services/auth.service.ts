import { Injectable } from '@angular/core';
import * as Oidc from 'oidc-client/lib/oidc-client.js';

@Injectable()
export class AuthService {
    public isLogged = '';

    config = {
        authority: 'http://localhost:8081',
        client_id: 'js',
        redirect_uri: 'http://localhost:4200/',
        response_type: 'id_token token',
        scope: 'openid profile api1',
        post_logout_redirect_uri : 'http://localhost:4200',
    };

    mgr: any;

    constructor() {
        this.mgr = new Oidc.UserManager(this.config);
        console.log(this.mgr);
    }

    public isLoggedIn(): void {
      this.mgr.getUser().then((user) => {
        if (user) {
          this.isLogged = 'Logged in !! :)';
        } else {
          this.isLogged = 'user didnt login :(';
        }
      });
    }

    public Login(login: string, password: string): void {
        this.mgr.signinRedirect();
    }

    public CheckApi(): void {
        this.mgr.getUser().then(function (user) {
            const url = 'http://localhost:5001/identity';

            const xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onload = function () {
                console.log(xhr.status, JSON.parse(xhr.responseText));
            };
            xhr.setRequestHeader('Authorization', 'Bearer ' + user.access_token);
            xhr.send();
        });
    }

    public Logout(): void {
        this.mgr.signOutRedirect();
    }
}

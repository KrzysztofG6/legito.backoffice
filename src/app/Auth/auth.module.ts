import { NgModule } from '@angular/core';

import { LoginComponent } from './Login/login.component';
import { AuthService } from './Services/auth.service';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [ ],
  providers: [ AuthService ]
})
export class AuthModule { }

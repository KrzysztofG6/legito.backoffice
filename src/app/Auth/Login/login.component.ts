import { Component } from '@angular/core';
import { AuthService } from '../Services/auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
    authService: AuthService;

    constructor (authService: AuthService) {
        this.authService = authService;
    }

    public Login(login: string, password: string): void {
        this.authService.Login(login, password);
    }
}
